package src.com.uitest.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.Color;

import java.util.List;

public class HomePage extends Page {
    private static final String HOME_PAGE_URL = "https://www.onliner.by/";

    private static final By NAVIGATION_MENU = By.xpath("//ul[@class='b-main-navigation']");
    private static final By NAVIGATION_MENU_SPAN = By.xpath("//ul[@class='b-main-navigation']/li[contains(@class, "
        + "'b-main-navigation__item')]/a/span[@class='b-main-navigation__text']");
    private static final By NAVIGATION_MENU_CAR_FLEA_MARKET = By.xpath("//li[@class='b-main-navigation__"
        + "item b-main-navigation__item_arrow']//span[contains(text(),'Автобарахолка')]");
    private static final By NAVIGATION_MENU_CAR_FLEA_MARKET_DROP_DOWN_LIST =
        By.xpath("//div[contains(@class,'b-main-navigation__dropdown_visible')]");

    public HomePage openPage() {
        driver.get(HOME_PAGE_URL);
        return this;
    }

    public Color getColorMenuNavigation() {
        waitForElementVisible(NAVIGATION_MENU);
        return Color.fromString(driver.findElement(NAVIGATION_MENU).getCssValue("background-color"));
    }

    public List<WebElement> getMenuNavigationList() {
        waitForElementVisible(NAVIGATION_MENU);
        return driver.findElements(NAVIGATION_MENU_SPAN);
    }

    public By getNavigationMenuCarFleaMarket() {
        return NAVIGATION_MENU_CAR_FLEA_MARKET;
    }

    public By getNavigationMenuCarFleaMarketDropDownList() {
        return NAVIGATION_MENU_CAR_FLEA_MARKET_DROP_DOWN_LIST;
    }

}
