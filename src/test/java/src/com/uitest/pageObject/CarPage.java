package src.com.uitest.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import src.com.uitest.utils.CurrencyEnum;

public class CarPage extends Page {
    private static final String HOME_PAGE_URL = "https://ab.onliner.by/";
    private static final By MIN_PRICE = By.xpath("//div[@class='selsgroup']/select[@name='min-price']");
    private static final By MAX_PRICE = By.xpath("//div[@class='selsgroup']/select[@name='max-price']");
    private static final By CAR_ROW = By.xpath("//tr[contains(@class,'carRow')]");
    private static final By PRICE_IN_CAR_ROW_ONE = By.xpath("//tr[contains(@class,'carRow')]//span[@class='price-primary']");

    public CarPage openPage(){
        driver.get(HOME_PAGE_URL);
        return this;
    }

    public CarPage openPage(String url){
        driver.get(url);
        return this;
    }

    public void setMinPrice(String minPrice){
        waitForElementVisible(MIN_PRICE);
        Select dropdown = new Select(driver.findElement(MIN_PRICE));
        dropdown.selectByValue(minPrice);
    }

    public void setMaxPrice(String maxPrice){
        waitForElementVisible(MAX_PRICE);
        Select dropdown = new Select(driver.findElement(MAX_PRICE));
        dropdown.selectByValue(maxPrice);
    }

    public void chooseCurrency(CurrencyEnum currencyEnum){
        driver.findElement(currencyEnum.getXPath()).click();
    }

    public By getCarRow(){
        return CAR_ROW;
    }

    public By getMaxPrice(){
        return MAX_PRICE;
    }

    public String getPriceInCarRowNumberOne(){
        return driver.findElement(PRICE_IN_CAR_ROW_ONE).getText();
    }
}
