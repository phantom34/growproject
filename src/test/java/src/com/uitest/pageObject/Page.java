package src.com.uitest.pageObject;

import src.com.driver.DriverSingleton;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Page {
    protected WebDriver driver;
    private static final int WAIT_FOR_ELEMENT_SECONDS = 10;

    protected Page(){
        this.driver = DriverSingleton.getWebDriverInstance();
    }

    protected void waitForElementVisible(By locator){
        new WebDriverWait(driver, WAIT_FOR_ELEMENT_SECONDS)
            .until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
    }

    private ExpectedCondition<Boolean> isAjaxFinished(){
        return (driver) -> {
            return (Boolean) ((JavascriptExecutor) driver).executeScript("return jQuery.active == 0");
        };
    }

    protected void waitForAjaxProcessed(){
        new WebDriverWait(driver, WAIT_FOR_ELEMENT_SECONDS).until(isAjaxFinished());
    }
}
