package src.com.uitest.utils;

public enum CarEnum {

    AUDI("Audi"),
    BMW("BMW"),
    CHRYSLER("Chrysler"),
    CITROEN("Citroen"),
    DODGE("Dodge"),
    FIAT("Fiat"),
    FORD("Ford"),
    HONDA("Honda"),
    HYUNDAI("Hyundai"),
    KIA("Kia"),
    MAZDA("Mazda"),
    MERCEDES("Mercedes"),
    MITSUBISHI("Mitsubishi"),
    NISSAN("Nissan"),
    OPEL("Opel"),
    PEUGEOT("Peugeot"),
    RENAULT("Renault"),
    ROVER("Rover");

    private String name;

    public String getName() {
        return name;
    }

    CarEnum(String name){
        this.name = name;
    }
}
