package src.com.uitest.utils;

import org.openqa.selenium.By;

public enum CurrencyEnum {
    BYN("р.", By.xpath("//a[contains(@data,'BYN')]")),
    DOL("$", By.xpath("//a[contains(@data,'USD')]")),
    EUR("€", By.xpath("//a[contains(@data,'EUR')]"));

    private String name;
    private By xPath;

    public String getName() {
        return name;
    }

    public By getXPath() {
        return xPath;
    }

    CurrencyEnum(String name, By xPath){
        this.name = name;
        this.xPath = xPath;
    }
}
