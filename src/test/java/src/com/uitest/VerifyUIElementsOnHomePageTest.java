package src.com.uitest;

import org.testng.Assert;
import org.testng.annotations.Test;
import src.com.uitest.steps.StepsForHomePage;

public class VerifyUIElementsOnHomePageTest {
    StepsForHomePage stepsForHomePage;

    @Test(groups = "UITest")
    public void verifyOrderInMenu() {
        stepsForHomePage = new StepsForHomePage();
        stepsForHomePage.openPage();
        Assert.assertTrue(stepsForHomePage.verifyOrderElementsInMenu(),"All good!");
    }

    @Test(groups = "UITest")
    public void verifyColorInMenu() {
        stepsForHomePage = new StepsForHomePage();
        stepsForHomePage.openPage();
        Assert.assertTrue(stepsForHomePage.verifyColorNavigationMenu("rgb(255, 221, 34)"), "All good!");
    }
}
