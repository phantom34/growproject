package src.com.uitest.steps;

import src.com.uitest.pageObject.CarPage;
import src.com.uitest.utils.CurrencyEnum;

public class StepsForCarPage extends CarPage {

    public void setMinAndMaxPrice(String minPrice, String maxPrice) {
        chooseCurrency(CurrencyEnum.BYN);
        setMinPrice(minPrice);
        setMaxPrice(maxPrice);
    }

    public boolean verifyPriceFirstCar(String minPrice, String maxPrice) {
        waitForAjaxProcessed();
        if (Integer.parseInt(getPriceInCarRowNumberOne().replace(" ", "")) >= Integer.parseInt(minPrice) && Integer.
            parseInt(getPriceInCarRowNumberOne().replace(" ", "")) <= Integer.parseInt(maxPrice)) {
            return true;
        } else {
            return false;
        }
    }
}
