package src.com.uitest.steps;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import src.com.uitest.pageObject.CarPage;
import src.com.uitest.pageObject.HomePage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StepsForHomePage extends HomePage {
    private static final  List<String> lIST_RIGHT_ORDER_ELEMENTS_IN_MENU = Arrays.asList("Каталог", "Новости",
        "Автобарахолка", "Дома и квартиры", "Услуги", "Барахолка", "Форум");
    private static final String CAR_IN_NAVIGATION_MENU = "//div[contains(@class,'b-main-navigation__dropdown_visible'"
        + ")]//span[contains(text(),'%s')]";
    private static final String URL_CAR_IN_NAVIGATION_MENU = "//div[contains(@class,'b-main-navigation__dropdown_visible'"
        + ")]//span[contains(text(),'%s')]/parent::a";

    public boolean verifyOrderElementsInMenu() {
        List<String> listElements = new ArrayList<String>();

        for (WebElement webElement : getMenuNavigationList()) {
            listElements.add(webElement.getText());
        }
        return listElements.containsAll(lIST_RIGHT_ORDER_ELEMENTS_IN_MENU);
    }

    public boolean verifyColorNavigationMenu(String color) {
        return getColorMenuNavigation().equals(Color.fromString(color));
    }

    public void navigateOnCarMarket() {
        new Actions(driver).moveToElement(driver.findElement(getNavigationMenuCarFleaMarket())).build().perform();
    }

    public CarPage getCarPage(String nameCar) {
        waitForElementVisible(getNavigationMenuCarFleaMarketDropDownList());
        if (driver.findElement(getNavigationMenuCarFleaMarketDropDownList()).isEnabled()) {
            return new CarPage().openPage(driver.findElement(getNavigationMenuCarFleaMarketDropDownList()).
                findElement(By.xpath(String.format(URL_CAR_IN_NAVIGATION_MENU, nameCar))).getAttribute("href"));
        }
        return new CarPage().openPage();
    }
}
