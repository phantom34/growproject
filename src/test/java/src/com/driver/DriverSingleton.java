package src.com.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.HashMap;
import java.util.Map;

import static src.com.driver.WebDriversEnum.*;

public class DriverSingleton {

    private static WebDriver instance;

    private DriverSingleton(){}

    public static WebDriver getWebDriverInstance(){
        if(instance != null){
            return instance;
        }
        return instance = initDriver();
    }

    private static WebDriver initDriver() {
        System.setProperty(CHROME_DRIVER.getKey(), CHROME_DRIVER.getValue());
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-features=EnableEphemeralFlashPermission");
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.content_settings.exceptions.plugins.*,*.setting", 1);
        options.setExperimentalOption("prefs", prefs);
        WebDriver driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        return driver;
    }

    public static void killDriver(){
        if(instance != null){
            try{
                instance.quit();
            }catch (Exception e){
                System.out.println("Cannot kill browser");
            }finally {
                instance = null;
            }
        }
    }
}
