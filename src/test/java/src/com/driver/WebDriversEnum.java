package src.com.driver;

public enum WebDriversEnum {
    FIREFOX_DRIVER("firefox", "webdriver.gecko.driver", "src//test//resources//geckodriver.exe"),
    CHROME_DRIVER("chrome", "webdriver.chrome.driver", "src//test//resources//chromedriver.exe");

    private String name;
    private String key;
    private String value;

    WebDriversEnum(String name, String key, String value) {
        this.name = name;
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
